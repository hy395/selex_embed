#This is code for training BindSpace model

##Requirements:

StarSpace installation: https://github.com/facebookresearch/StarSpace  
R packages: Biostrings, Matrix, parallel, data.table, glmnet, doParallel  
use 4 cores by default.

##data:
Filtered HT-SELEX probes and corresponding labels. 

##scripts:
generateBOW.sh: generate bag of kmers from DNA sequence.(takes ~1h using 4 cores on a mac pro)  
train_bindspace.sh: script for training BindSpace model using Starspace.(estimated ~17h with 4 cores)  
train_bindspaceplus.R script for training BindSpace+ model.

##train:
1) cd selex_embed  
2) ./scripts/generateBOW.sh  
2) ./scripts/train_bindspace.sh  
3) Rscript ./scripts/train_bindspaceplus.R

# evaluate:
For making prediction with trained models, see https://bitbucket.org/hy395/bindspace

Han Yuan 2018
