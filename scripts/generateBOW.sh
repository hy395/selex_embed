#!/bin/bash

# 1. scripts for generating bag of kmers
Rscript scripts/fasta2bow.R data/train_seqs.fasta data/train_labels.txt data/train.bow TRUE
Rscript scripts/fasta2bow.R data/test_seqs.fasta data/test_labels.txt data/test.bow TRUE
cat data/train.bow data/test.bow > data/all.bow

