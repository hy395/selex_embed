#!/bin/bash
# 2. run StarSpace to train on all SELEX data

trainFile="data/all.bow"
lr=0.2
dim=300
maxNegSamples=15
negSearchLimit=$((${maxNegSamples}*3))
dropoutLHS=0
epoch=50
outdir="model/"
prefix="${outdir}/model_${lr}_${dim}_${maxNegSamples}_${dropoutLHS}"
mkdir -p ${outdir}

# train
echo "Start to train:"
starspace train \
  -trainFile ${trainFile} \
  -model ${prefix}.model \
  -initRandSd 0.01 \
  -adagrad true \
  -ngrams 1 \
  -loss hinge \
  -lr ${lr} \
  -epoch ${epoch} \
  -thread 4 \
  -dim ${dim} \
  -negSearchLimit ${negSearchLimit} \
  -maxNegSamples ${maxNegSamples} \
  -trainMode 0 \
  -label "#" \
  -dropoutLHS ${dropoutLHS} \
  -similarity dot \
  -verbose false \
  -normalizeText 0 > ${prefix}.log 2>&1
