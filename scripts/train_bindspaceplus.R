library(data.table)
library(Biostrings)
library(Matrix)
library(parallel)
library(doParallel)
registerDoParallel(5)

load("data/dict_8mer2wc8mer.rdt")

#############
# functions #
#############
makeFeats <- function(seqs, k, rc=F) {
  a <- oligonucleotideFrequency(seqs, k)
  feats <- a
  if (rc) {
    seqs_rc <- reverseComplement(seqs)
    b <- oligonucleotideFrequency(seqs_rc, k)
    feats <- a+b
  }
  return(feats)
}

get.time <- function () {
  return(as.numeric(format(Sys.time(), "%s")))
}

########################################
# compute embedding of training probes #
########################################
vocab_8mer <- colnames(oligonucleotideFrequency(DNAStringSet("AAAAAAAA"), 8))
pairwise.kmers <- pairwise.kmers[vocab_8mer,]
vocab_8mer_wc <- colnames(pairwise.kmers)

# Use BindSpace representation for training data
tmp <- fread("model/model_0.2_300_15_0.model.tsv") # this is the embedding matrix
info <- fread("data/labels_table.txt")
tmp <- data.frame(tmp)
rownames(tmp) <- tmp[,1]
tmp <- tmp[,-1]
m <- data.matrix(tmp)
kmer_m <-  m[-grep("#", rownames(m)),]
kmer_m <- kmer_m[vocab_8mer_wc,] #embedding for kmers

# compute 300 dimensional representation of training probes
input <- readDNAStringSet("data/all_seqs.fasta")
labels <- read.table("data/all_labels.txt", sep="\t", stringsAsFactors = F)[,1]
labels <- gsub("TF_","", gsub(" Family.*","",labels))
train <- matrix(NA, length(input), 300, dimnames=list(names(input),NULL))
N <- length(input) # input sequences size
batch_size <- 5000
peak_size <- nchar(input[1])
batches <- ceiling(N/batch_size)
p <- 0.5 # normalization factor, same as training
print(paste0("total ",N, " sequences."))
time.start <- get.time()
for (i in 1:batches) {
  # generate batch index
  start <- (i-1)*batch_size + 1
  end <- min(i*batch_size, N)
  # generate feature matrix and labels
  tmp <- Matrix(makeFeats(input[start:end], 8), sparse=T)
  feats_tmp <- tmp %*% pairwise.kmers
  feats_tmp[feats_tmp!=0] <- 1 # only consider unique kmers
  tmp_count <- rowSums(feats_tmp) # number of unique kmers
  tmp_embed <- as.matrix(feats_tmp %*% kmer_m / (tmp_count^p)) # embedding for probe, normalized by # kmers and factor p, this is same as starspace
  train[start:end, ] <- tmp_embed
  time.end <- get.time()
  print(paste0("Process ",min(i*batch_size, N), " sequences takes ", round( (time.end-time.start)/60, 3), " min."))
}

saveRDS(train,  file="model/train_embed.rds")

################
# ridge models #
################
library(glmnet)

train <- readRDS("model/train_embed.rds")
labels <- read.table("data/all_labels.txt", sep="\t", stringsAsFactors = F)[,1]
labels <- gsub("TF_","", gsub(" Family.*","",labels))

tfs <- unique(labels)
tfs <- tfs[tfs!="neg"]

dir.create("model/bindspace_plus/", recursive = T)
for (i in 1:length(tfs)) {
  pos_tmp <- train[labels==tfs[i],]
  neg_tmp <- train[labels!=tfs[i],]
  neg_tmp <- neg_tmp[sample(1:nrow(neg_tmp),nrow(pos_tmp)*10),]
  # train
  x_tmp <- rbind(pos_tmp,neg_tmp)
  y_tmp <- factor(c(rep(1, nrow(pos_tmp)), rep(0, nrow(neg_tmp))))
  fit <- cv.glmnet(x=x_tmp, y=y_tmp, family="binomial", type.measure="deviance", nfolds=5, alpha=0, parallel=T)
  saveRDS(fit, paste0("model/bindspace_plus/",tfs[i],"_model.rds"))
  print(i)
}

